
//  express.Router() method allows access to HTTP methods
const express = require("express");

// const taskControllers = require("../controllers/taskControllers");
const router = express.Router();

const taskController = require("../controllers/taskController")



// Create = task routes

router.post("/addTask", taskController.createTaskController)

// Get all task
router.get("/allTask", taskController.getAllTasksController)


// delete task
router.delete("/deleteTask/:taskId", taskController.deleteTaskController);



// get specific task
router.get("/:taskId", taskController.getSpecificTask)


// update specific task

router.put("/:taskId/completed", taskController.putTaskController)


router.put("/:id/archive", (req, res) => {
	taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 

router.patch("/updateTask/:taskId", taskController.updateTaskNameController);

module.exports = router;




